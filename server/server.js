require('./config/config');
const express = require('express');
const bodyParser = require('body-parser');

const router = express();

// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get('/usuario', (req, res) => {
  res.json('Funciona get');
});
router.post('/usuario', (req, res) => {
  let body = req.body;
  if (body.name === undefined) {
    res.status(404).send({
      ok: false,
      message: 'El nombre es necesario',
    });
  }
  let persona = req.body;
  res.json(persona);
});
router.put('/usuario/:id', (req, res) => {
  let id = req.params.id;
  res.json(id);
});
router.delete('/usuario', (req, res) => {
  res.json('Funciona delete');
});
router.listen(process.env.PORT, () => {
  console.log('Conexion exitosa');
});
